// NOTE Angular component syntax etc over here.

/**
 * @description - lets assume that we have some large operation that we wants to perform in background of angular
 * application at that time we required something like worker.
 */
async function process() {
    // Here, we are checking that client system supporting a worker or not by below
    if (typeof Worker !== 'undefined') {
        // Create a new
        const worker = new Worker('./app.worker', { type: 'module' });
        worker.onmessage = ({ data }) => {
            console.log(`page got message: ${data}`);
        };
        worker.postMessage('hello');
    } else {
        // Web workers are not supported in this environment.
        // You should add a fallback so that your program still executes correctly.
    }
}
