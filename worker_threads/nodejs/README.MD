## Worker Threads in node.js
- Worker threads is an exciting and useful module if you need to do CPU-intensive tasks in your Node.js application. 
It’s like threads without shared memory and, thus, without the potential race conditions they introduce. 
Since the worker_threads module became stable in Node.js v12 LTS, you should feel secure using it in production-grade apps.

- Without worker thread application like below:
    - One process
    - One thread
    - One event loop
    - One JS Engine Instance
    - One Node.js Instance

- With worker thread application like below:
    - One process
    - Multiple threads
    - One event loop per thread
    - One JS Engine Instance per thread
    - One Node.js Instance per thread
    
### What makes the worker thread special:
- `ArrayBuffers` to transfer memory from one thread to another
- `SharedArrayBuffer` that will be accessible from either thread. It lets you share memory between threads (limited to binary data).
- `Atomics` available, it lets you do some processes concurrently, more efficiently and allows you to implement conditions variables in JavaScript
- `MessagePort`, used for communicating between different threads. It can be used to transfer structured data, memory regions and other MessagePorts between different Workers.
- `MessageChannel` represents an asynchronous, two-way communications channel used for communicating between different threads.
- `WorkerData` is used to pass startup data. An arbitrary JavaScript value that contains a clone of the data passed to this thread’s Worker constructor. The data is cloned as if using `postMessage()`
