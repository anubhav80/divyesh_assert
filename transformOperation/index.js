'use strict';
const {removeFieldsByKey, removeFieldsByValue} = require('./transformJSON');
const {differenceInTwoObjects} = require('./differenceInTwoObjects');

/**
 * @module Manage the transformOperation module
 */
module.exports = {
	removeFieldsByKey,
	removeFieldsByValue,
	differenceInTwoObjects
};
