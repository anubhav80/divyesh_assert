'use strict';

/**
 * @description This will useful to remove the specific field by keyName from n level of JSON
 *
 * @param {Object | Array} objectToProcess - The array or object from we are going to remove the specific key
 * @param {String} keyToRemove - Key to remove from the JSON Data
 *
 * @return - The transform object after removing the specific field by key from JSON Data.
 */
const removeFieldsByKey = (objectToProcess, keyToRemove) => {
	return JSON.parse(JSON.stringify(objectToProcess, (key, value) => {
		return key === keyToRemove ? undefined : value;
	}));
};

/**
 * @description This will useful to remove the specific field by string value from n level of JSON
 *
 * @param {Object | Array} objectToProcess - The array or object from we are going to remove the specific value
 * @param {String} valueToRemove - Value to remove from the JSON Data
 *
 * @return - The transform object after removing the specific field by value from JSON Data.
 */
const removeFieldsByValue = (objectToProcess, valueToRemove) => {
	return JSON.parse(JSON.stringify(objectToProcess, (key, value) => {
		return value === valueToRemove ? undefined : value;
	}));
};

/**
 * @module Manage the transformJSON module
 */
module.exports = {
	removeFieldsByKey,
	removeFieldsByValue
};
