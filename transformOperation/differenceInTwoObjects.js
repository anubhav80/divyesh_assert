'use strict';
const _ = require('lodash');

/**
 * @description - This will return the pure difference from two object.
 *
 * @param {Object} baseObject - The base object
 * @param {Object} compareWithObject - The object going to compare with base object.
 *
 * @return {Object} - Return the pure difference
 */
const differenceInTwoObjects = function (baseObject, compareWithObject) {
	function difference(baseObject, compareWithObject) {
		return _.transform(compareWithObject, function (result, value, key) {
			if (!_.isEqual(value, baseObject[key])) {
				if (_.isObject(value) && _.isObject(baseObject[key])) {
					result[key] = difference(baseObject[key], value);
				} else {
					result[key] = value;
				}
			}
		});
	}
	return difference(baseObject, compareWithObject);
};

module.exports = {
	differenceInTwoObjects
};
