'use strict';
const QJobs = require('qjobs');

/**
 * @module Manage the job queue module
 */
module.exports = {
	/**
	 * @description - Responsible to create the background queue instance
	 *
	 * @param {string} messageOnStart - The message that you want's to log on a starting of the queue process
	 * @param {string} messageOnEnd - The message that you want's to log on a end of the queue process
	 * @param {number} maxConcurrency - The maximum concurrent job process
	 * @param {boolean} destroyInstanceAtEnd - Flag to indicate that queue job instance needs to destroy or not
	 *
	 * @default maxConcurrency 25
	 * @default destroyInstanceAtEnd true
	 *
	 * @return - Instance of QJobs
	 */
	createBackGroundJobQueue: (
		messageOnStart = `Background job started at ${new Date()}`,
		messageOnEnd = `Background job completed at ${new Date()}`,
		maxConcurrency = 25,
		destroyInstanceAtEnd = true
	) => {
		let job = new QJobs({ maxConcurrency });
		job.on('start', function () {
			console.log(`createBackGroundJobQueue: ${messageOnStart}`);
		});
		job.on('end', function () {
			console.log(`createBackGroundJobQueue: ${messageOnEnd}`);
			if (destroyInstanceAtEnd) {
				setTimeout(() => {
					job = null;
				});
			}
		});
		job.on('pause',function(since) {
			console.log(`createBackGroundJobQueue: in pause state since ${since}`);
		});

		job.on('unpause',function() {
			console.log('createBackGroundJobQueue: end of pause and resume again');
		});
		return job;
	}
};
