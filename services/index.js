'use strict';
const {createBackGroundJobQueue} = require('./background.job');

/**
 * @module Manage the service module
 */
module.exports = {
	createBackGroundJobQueue
};
