'use strict';

/**
 * @description This util will helpful while doing a parallel DML in firebase and sequalize db.
 * At that time, this util will helps to manage the failure in db operations and roll back.
 *
 * @param {Object|Class} dbModel - The sequalize database model or class.
 * @param {*} operationOnDb - The db operation wants to perform in parallel with firebase operation.
 * @param {*} operationOnFirebase - The firebase operation wants to perform.
 * @param {Object} conditionForDb -  Where clause going to use for db rollback and retrieve the previous version on db record.
 * @param {Object} operationFlags - The operation flags which determines the operations
 *
 * @async
 *
 * @return firebase operation output
 */
const updateAndManageFailure = async (
	dbModel,
	operationOnDb,
	operationOnFirebase,
	conditionForDb,
	operationFlags = {}
) => {
	try {
		let fetchedRecord;
		operationFlags.isFetchRequire = operationFlags.isForDelete || operationFlags.isFetchRequire;
		if (operationFlags.isFetchRequire) {
			fetchedRecord = await dbModel.findOne(conditionForDb);
		}
		await operationOnDb;
		try {
			return await operationOnFirebase;
		} catch (e) {
			console.log('An error occurred while upserting/deleting in firebase', e);
			if (operationFlags.isForDelete) {
				await dbModel.create(fetchedRecord);
			} else if (operationFlags.isForCreate) {
				await dbModel.delete(conditionForDb);
			} else {
				await dbModel.update(fetchedRecord.toJSON(), conditionForDb);
			}
			throw e;
		}
	} catch (e) {
		console.log('An error occurred while manage the update and rollback in failure', e);
		throw new Error('An error occurred while duplicating the firebase record operation in sequalize');
	}
};

/**
 * @module firebaseAndSequalizeParallelDML
 */
module.exports = {
	updateAndManageFailure
}
