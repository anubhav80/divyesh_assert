## utils
- Includes the utilities functions.

### How to use `firebaseAndSequalizeParallelDML.updateAndManageFailure`
Example:- Here, we want to perform various operation in parallel with firebase and sequalize.
```javascript
const firebaseAndSequalizeParallelDML = require('./firebaseAndSequalizeParallelDML');
const {ContactModel} = require('../models');

const updateContact = async (req, res, next) => {
  // Here, we are update the first_name for contact_id:10 in sequal as well as firebase and manage the rollback in failure.
  const firebaseResult = await  firebaseAndSequalizeParallelDML.updateAndManageFailure(
    ContactModel,
    ContactModel.update({first_name: 'Divyesh'}, {contact_id: 10}),
    document.ref.update({first_name: 'Divyesh'}),
    {contact_id: 10},
    {isFetchRequire: true}
  );
};

const createContact = async (req, res, next) => {
  /* Create the new record for contact. And if anything goes wrong we can remove that contact from db by matching email, 
     If sequal successfully create record and error occurred while creating in firebase.
   */
  const firebaseResult = await  firebaseAndSequalizeParallelDML.updateAndManageFailure(
    ContactModel,
    ContactModel.create(_.cloneDeep(req.body)),
    documentReference.set(req.body),
    {contact_email: req.body.email},
    {isForCreate: true}
  );
};

const deleteContact = async (req, res, next) => {
  /* Delete the record for contact. And if anything goes wrong we can create that contact in db with previous version of record, 
     If sequal successfully delete record and error occurred while deleting in firebase.
   */
  const firebaseResult = await  firebaseAndSequalizeParallelDML.updateAndManageFailure(
    ContactModel,
    ContactModel.destroy({contact_id: 10}),
    contact.doc(documentSnapshot.id).delete(),
    {contact_id: 10},
    {isForDelete: true, isFetchRequire: true}
  );
};
```
